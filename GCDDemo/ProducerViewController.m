//
//  ProducerViewController.m
//  GCDDemo
//
//  Created by amttgroup on 15-5-22.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ProducerViewController.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
NSString * const kHeaderDelimiterString = @"\r\n\r\n";
@interface ProducerViewController()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic) NSString * imagePath;

@end
@implementation ProducerViewController

- (NSData*) headerDelimiter
{
    static NSData * data;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = [kHeaderDelimiterString dataUsingEncoding:NSUTF8StringEncoding];
    });
    return data;
}

- (dispatch_fd_t) connectToHostName:(NSString*) hostName port:(int)  port
{
    int s;
    struct sockaddr_in sa;
    struct hostent * he;
    
    if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        NSAssert(NO, @"socket failed:%d",errno);
    }
    
    bzero(&sa, sizeof sa);
    
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    
    if ((he = gethostbyname([hostName UTF8String])) == NULL) {
        NSAssert(NO, @"gethostbyname failure:%d",errno);
    }
    
    bcopy(he->h_addr_list[0], &sa.sin_addr, he->h_length);
    
    if (connect(s, (struct sockaddr*)&sa, sizeof sa) < 0) {
        close(s);
        NSAssert(NO, @"connect failed:%d",errno);
    }
    return s;
}


- (NSString*) ouputFilePathForPath:(NSString*) path
{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:[path lastPathComponent]];
}

- (void) writeToChannel:(dispatch_io_t) channel data:(dispatch_data_t) writeData queue:(dispatch_queue_t) queue
{
    dispatch_io_write(channel, 0, writeData, queue, ^(bool done, dispatch_data_t data, int error) {
        NSAssert(!error, @"File write Error%d",error);
        size_t unwritenDataLength = 0;
        if (data) {
            unwritenDataLength = dispatch_data_get_size(data);
        }
        NSLog(@"Wrote %zu bytes",dispatch_data_get_size(writeData) - unwritenDataLength);
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfFile:self.imagePath]];
            self.imageView.image = image;
            [self.activity stopAnimating];
        });
    });
}

- (void) handleDoneWithChannels:(NSArray*) channels
{
    NSLog(@"Done Downloading");
    for (dispatch_io_t channel in channels) {
        dispatch_io_close(channel, 0);
    }
}

- (dispatch_data_t) findHeaderInData:(dispatch_data_t) newData
                        previousData:(dispatch_data_t *) previousData
                        writeChannel:(dispatch_io_t) writeChannel
                               queue:(dispatch_queue_t) queue
{
    *previousData = dispatch_data_create_concat(*previousData, newData);
    dispatch_data_t mappedData = dispatch_data_create_map(*previousData, NULL, NULL);
    
    __block dispatch_data_t headerData;
    __block dispatch_data_t bodyData;
    
    dispatch_data_apply(mappedData, ^bool(dispatch_data_t region, size_t offset, const void *buffer, size_t size) {
        NSData * search = [[NSData alloc] initWithBytesNoCopy:(void*)buffer length:size freeWhenDone:NO];
        NSRange r = [search rangeOfData:[self headerDelimiter] options:0 range:NSMakeRange(0, search.length)];
        
        if (r.location != NSNotFound) {
            headerData = dispatch_data_create_subrange(region, 0, r.location);
            size_t body_offset = NSMaxRange(r);
            size_t body_size = size - body_offset;
            bodyData = dispatch_data_create_subrange(region, body_offset, body_size);
        }
        return false;
    });
    
    if (bodyData) {
        [self writeToChannel:writeChannel data:bodyData queue:queue];
    }
    
    return headerData;
}

- (void) printHeader:(dispatch_data_t) headerData
{
    printf("\nHeader:\n\n");
    dispatch_data_apply(headerData, ^bool(dispatch_data_t region, size_t offset, const void *buffer, size_t size) {
        fwrite(buffer, size, 1, stdout);
        return true;
    });
    printf("\n\n");
}

- (void) readFromChannel:(dispatch_io_t) readChannel writeToChannel:(dispatch_io_t) writeChannel queue:(dispatch_queue_t) queue
{
    __block dispatch_data_t previousData = dispatch_data_empty;
    __block dispatch_data_t headerData;
    dispatch_io_read(readChannel, 0, SIZE_MAX, queue, ^(bool done, dispatch_data_t data, int error) {
        NSAssert(!error, @"Server read Error:%d",error);
        
        if (done) {
            [self handleDoneWithChannels:@[writeChannel,readChannel]];
        }
        else {
            if (!headerData) {
                headerData = [self findHeaderInData:data previousData:&previousData writeChannel:writeChannel queue:queue];
                if (headerData) {
                    [self printHeader:headerData];
                }
            }
            else {
                [self writeToChannel:writeChannel data:data queue:queue];
            }
        }
    });
}

- (dispatch_data_t) requestDataForHostName:(NSString*) hostName path:(NSString*) path
{
    NSString * getString = [NSString stringWithFormat:@"GET %@ HTTP/1.1\r\nHost: %@\r\n\r\n",path,hostName];
    NSLog(@"Request:\n%@",getString);
    NSData * getData = [getString dataUsingEncoding:NSUTF8StringEncoding];
    return dispatch_data_create(getData.bytes,getData.length, NULL, DISPATCH_DATA_DESTRUCTOR_DEFAULT);
}


- (void) HTTPDownloadContentsFromHostName:(NSString*) hostName port:(int) port path:(NSString*) path
{
    dispatch_queue_t queue = dispatch_get_main_queue();
    dispatch_fd_t socket = [self connectToHostName:hostName port:port];
    
    dispatch_io_t serverChannel = dispatch_io_create(DISPATCH_IO_STREAM, socket, queue, ^(int error) {
        NSAssert(!error, @"Failed socket:%d",error);
        NSLog(@"Closing connectino");
        close(socket);
    });
    
    dispatch_data_t requestData = [self requestDataForHostName:hostName path:path];
    NSString * writePath = [self ouputFilePathForPath:path];
    self.imagePath = writePath;
    NSLog(@"Writing to %@",writePath);
    
    dispatch_io_t fileChannel = dispatch_io_create_with_path(DISPATCH_IO_STREAM, [writePath UTF8String], O_WRONLY|O_CREAT|O_TRUNC, S_IRWXU, queue, nil);
    dispatch_io_write(serverChannel, 0, requestData, queue, ^(bool done, dispatch_data_t data, int error) {
        NSAssert(!error, @"Server write error:%d",error);
        if (done) {
            [self readFromChannel:serverChannel writeToChannel:fileChannel queue:queue];
        }
    });
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self.activity startAnimating];
    
//    [self HTTPDownloadContentsFromHostName:@"upload.wikimedia.org"
//                                      port:80
//                                      path:@"/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg"];
    [self HTTPDownloadContentsFromHostName:@"b.hiphotos.baidu.com"
                                      port:80
                                      path:@"/image/h%3D200/sign=6b1f5afc6f63f624035d3e03b745eb32/b90e7bec54e736d1b62e5dcd9f504fc2d5626932.jpg"];
}

@end
