//
//  MainViewController.m
//  GCDDemo
//
//  Created by amttgroup on 15-5-22.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ((UIViewController*)segue.destinationViewController).title = ((UITableViewCell*)sender).textLabel.text;
}
@end
