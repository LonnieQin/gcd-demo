//
//  DispatchViewController.m
//  GCDDemo
//
//  Created by amttgroup on 15-5-22.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DispatchViewController.h"
#import "LQQueue.h"
#import "RNQueue.h"
#import <dispatch/time.h>
@interface DispatchViewController()
@property (nonatomic) dispatch_semaphore_t semaphore;
@property (nonatomic) dispatch_queue_t pendingQueue;
@property (nonatomic) dispatch_queue_t workQueue;
@property (strong, nonatomic) IBOutlet UILabel *inQueueLabels;
@property (strong, nonatomic) IBOutletCollection(UIProgressView) NSArray *progressViews;
@property (nonatomic) int _pendingJobCount;
@end
@implementation DispatchViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.semaphore = dispatch_semaphore_create([self.progressViews count]);
    self.pendingQueue = RNQueueCreateTagged("ProducerConsumer.pending", DISPATCH_QUEUE_SERIAL);
    self.workQueue = RNQueueCreateTagged("ProducerConsumer.work", DISPATCH_QUEUE_CONCURRENT);
    NSLog(@"%@ %@ %@",self.progressViews[0],self.progressViews[1],self.progressViews[2]);
    /*
    self.pendingQueue = LQQueueCreateTagged("ProducerConsumer.pending", DISPATCH_QUEUE_SERIAL);
    self.workQueue = LQQueueCreateTagged("ProducerConsumer.work", DISPATCH_QUEUE_CONCURRENT);
    NSLog(@"%d %@ %@ %@",self.progressViews.count,self.pendingQueue,self.workQueue,self.pendingQueue);
     */
}

- (void) adjustPendingJobCountBy:(NSInteger) value
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self._pendingJobCount += value;
        self.inQueueLabels.text = [NSString stringWithFormat:@"%d",self._pendingJobCount];
    });
}


- (UIProgressView*) reserveProgressView
{
    RNAssertQueue(self.pendingQueue);
    //LQAssertQueue(self.pendingQueue);
    __block UIProgressView * curProgressView;
    dispatch_async(dispatch_get_main_queue(), ^{
        for (UIProgressView * progressview in self.progressViews) {
            if (progressview.isHidden) {
                curProgressView = progressview;
                break;
            }
        }
        curProgressView.hidden = NO;
        curProgressView.progress = 0;
    });
    NSAssert(curProgressView,@"There should always be one availabel here");
    return curProgressView;
}

- (IBAction)process:(id)sender {
    RNAssertMainQueue();
    //LQAssertMainQueue();
    [self adjustPendingJobCountBy:1];
    dispatch_async(self.pendingQueue, ^{
        dispatch_semaphore_wait(self.semaphore,DISPATCH_TIME_FOREVER);
        UIProgressView * avalableProgressView = [self reserveProgressView];
        dispatch_async(self.workQueue, ^{
            [self performWorkWithProgressView:avalableProgressView];
            [self releaseProgressView:avalableProgressView];
            [self adjustPendingJobCountBy:-1];
            dispatch_semaphore_signal(self.semaphore);
        });
    });
}


- (void) performWorkWithProgressView:(UIProgressView*) progressView
{
    RNAssertQueue(self.workQueue);
    //LQAssertQueue(self.workQueue);
    
    for (NSUInteger p = 0; p <= 100; ++p) {
        dispatch_async(dispatch_get_main_queue(), ^{
            progressView.progress = p/100.0;
        });
        usleep(10000);
    }
    
}







- (void) releaseProgressView:(UIProgressView*) progressView
{
    LQAssertQueue(self.workQueue);
    dispatch_async(dispatch_get_main_queue(), ^{
        progressView.hidden = YES;
    });
}

@end
