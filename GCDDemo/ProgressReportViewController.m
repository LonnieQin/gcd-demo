//
//  ProgressReportViewController.m
//  GCDDemo
//
//  Created by amttgroup on 15-5-22.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ProgressReportViewController.h"
@interface ProgressReportViewController()
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;

@end
@implementation ProgressReportViewController

- (IBAction)go:(id)sender {
    dispatch_source_t source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_get_main_queue());
    __block long totalComplete = 0;
    dispatch_source_set_event_handler(source, ^{
        long value = dispatch_source_get_data(source);
        totalComplete += value;
        self.progressView.progress = (CGFloat)totalComplete/100.0f;
    });
    dispatch_resume(source);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        for (int i = 0; i <= 100; ++i) {
            dispatch_source_merge_data(source, 1);
            usleep(20000);
        }
    });
}
@end
