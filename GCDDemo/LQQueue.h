//
//  LQQueue.h
//  GCDDemo
//
//  Created by amttgroup on 15-5-22.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <Foundation/Foundation.h>

void LQQueueTag(dispatch_queue_t q);

dispatch_queue_t LQQueueCreateTagged(const char * label,dispatch_queue_attr_t attr);

BOOL LQQueueCurrentIsTaggedQueue(dispatch_queue_t q);

dispatch_queue_t LQQueueGetCurrentTagged();

BOOL LQQueueCurrentIsMainQueue();

void LQQueueSafeDispatchSync(dispatch_queue_t q,dispatch_block_t block);

#define LQAssertQueue(q) NSAssert(LQQueueCurrentIsTaggedQueue(q),@"Must run on queue: " #q)

#define LQAssertMainQueue() NSAssert(LQQueueCurrentIsMainQueue(),@"Must run on main queue");

 