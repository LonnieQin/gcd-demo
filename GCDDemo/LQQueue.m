//
//  LQQueue.m
//  GCDDemo
//
//  Created by amttgroup on 15-5-22.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "LQQueue.h"
 
static const char sQueueTagKey;

void LQQueueTag(dispatch_queue_t q)
{
    dispatch_queue_set_specific(q, &sQueueTagKey, (__bridge void*)q, NULL);
}

dispatch_queue_t LQQueueCreateTagged(const char * label,dispatch_queue_attr_t attr)
{
    dispatch_queue_t q = dispatch_queue_create(label, attr);
    LQQueueTag(q);
    return q;
}

dispatch_queue_t LQQueueGetCurrentTagged()
{
    return (__bridge dispatch_queue_t)dispatch_get_specific(&sQueueTagKey);
}


BOOL LQQueueCurrentIsTaggedQueue(dispatch_queue_t q)
{
    return (LQQueueGetCurrentTagged() == q);
}

BOOL LQQueueCurrentIsMainQueue()
{
    return [NSThread isMainThread];
}

void LQQueueSafeDispatchSync(dispatch_queue_t q,dispatch_block_t block)
{
    if (LQQueueCurrentIsTaggedQueue(q)) {
        block();
    } else {
        dispatch_async(q, block);
    }
}
